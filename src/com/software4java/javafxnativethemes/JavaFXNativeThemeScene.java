package com.software4java.javafxnativethemes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import java.io.IOException;

/**
 * User: TB
 * Date: 15.04.12
 * Time: 00:24
 * To change this template use File | Settings | File Templates.
 */
public class JavaFXNativeThemeScene
{

    private Scene scene;
    @FXML
    private static ChoiceBox skinChooser;
    private Skins oldSkin;
    private Parent root;


    public JavaFXNativeThemeScene()
    {
        try
        {
            root = FXMLLoader.load(getClass().getResource("/com/software4java/javafxnativethemes/NativeThemeDemo.fxml"));
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        this.scene = new Scene(root);

        createSkinChooser();


        /*
        ((Button)this.scene.lookup("#skinRefreshButton").)setOnAction(new EventHandler<>()
        {

        });
        */

        Object o = this.scene.lookup("#myContextMenu");
        System.out.println("o = " + o);

        ((Button) this.scene.lookup("#contextMenuButton")).setOnAction(new EventHandler<ActionEvent>()
        {
            public void handle(ActionEvent actionEvent)
            {
                ((Button) actionEvent.getSource()).getContextMenu().show(((Button) actionEvent.getSource()), Side.BOTTOM, 0, 0);
            }
        });

        ((Button) this.scene.lookup("#skinRefreshButton")).setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                refreshSkin();
            }
        });

        if (skinChooser != null)
        {
            String os = System.getProperty("os.name");
            os = os.toLowerCase().trim();
            System.out.println("os = " + os);
            if (os.startsWith("win"))
            {
                skinChooser.getSelectionModel().select(0);
            }
            else if(os.startsWith("mac"))
            {
                skinChooser.getSelectionModel().select(1);
            }
            else
            {
                skinChooser.getSelectionModel().select(3);
            }
        }
    }

    private void refreshSkin()
    {
        useSkin(this.oldSkin);
    }


    private void useSkin(Skins skin)
    {
        System.out.println("use skin = " + skin);
        this.scene.getStylesheets().clear();
        this.scene.getStylesheets().add(skin.getCssFile());
        this.oldSkin = skin;
    }

    private void createSkinChooser()
    {

        //File skins = new File("layout/test");
        this.skinChooser = (ChoiceBox) this.scene.lookup("#skinChooser");

        this.skinChooser.setItems(FXCollections.observableArrayList(Skins.values()));

        this.skinChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener()
        {
            public void changed(ObservableValue observableValue, Object oldV, Object newV)
            {
                Skins skin = (Skins) newV;
                useSkin(skin);
            }
        });
    }


    public Scene getScene()
    {
        return scene;
    }
}
