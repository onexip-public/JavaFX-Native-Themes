package com.software4java.javafxnativethemes;

import com.sun.javafx.tk.Toolkit;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by IntelliJ IDEA.
 * User: TB
 * Date: 01.03.12
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public class SwingAndJavaFXNativeThemeDemo extends JPanel
{

    private JFrame frame;
    private JButton bDefault;

    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (UnsupportedLookAndFeelException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        new SwingAndJavaFXNativeThemeDemo();
    }

    public SwingAndJavaFXNativeThemeDemo()
    {
        frame = new JFrame("JavaFX Native Themes");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                System.out.println("e = " + e);
            }
        });

        SwingNativeThemeDemoPanel panelSwing = new SwingNativeThemeDemoPanel();


        this.setLayout(new MigLayout("ins 0 10 10 10", "[grow,fill][grow,fill]", "[grow,fill, top]"));
        this.add(panelSwing, "");


        final JFXPanel p = new JFXPanel();
        this.add(p, "");

        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Toolkit.getToolkit().init();
                    Scene scene = SwingAndJavaFXNativeThemeDemo.this.createGUIFX();
                    p.setScene(scene);
                }
                catch (Exception e)
                {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });

        frame.add(this);

        getRootPane().setDefaultButton(bDefault);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private Scene createGUIFX()
    {
        return new JavaFXNativeThemeScene().getScene();
    }


}
