package com.software4java.javafxnativethemes;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: TB
 * Date: 20.02.13
 * Time: 23:15
 * To change this template use File | Settings | File Templates.
 */
public class SwingNativeThemeDemo extends JFrame
{
    public SwingNativeThemeDemo()
    {
        this.setTitle("JavaFX Native Themes");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                System.out.println("e = " + e);
            }
        });

        SwingNativeThemeDemoPanel panelSwing = new SwingNativeThemeDemoPanel();

        this.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill, top]"));
        this.add(panelSwing, "grow");

        //  getRootPane().setDefaultButton(bDefault);

        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (UnsupportedLookAndFeelException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        new SwingNativeThemeDemo();
    }
}