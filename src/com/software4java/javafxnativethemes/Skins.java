package com.software4java.javafxnativethemes;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: TB
 * Date: 15.04.12
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */
public enum Skins
{
    //WINDOWS_8("Windows 8", "win/Windows-8.css"),
    //WINDOWS_7_GLASS("Windows 7 Glass", "win/win7glass.css"),
    WINDOWS_7("Windows 7", "win/Windows-7.css"),
    MAC_OSX_107("Mac OSX 10.8", "mac/Mac-OSX-108.css"),
    CASPIAN("JavaFX 8 Caspian", "cross/caspian/caspian.css"),
    MODENA("JavaFX 8 Modena", "cross/modena/modena.css");

    private String name;
    private String cssFile;
    private Logger logger = Logger.getLogger(getClass().getName());

    private Skins(String name, String cssFile)
    {
        this.name = name;
        this.cssFile = cssFile;
    }

    public String getName()
    {
        return name;
    }

    public String getCssFile()
    {
        try
        {
            return new File("skins/" + cssFile).toURL().toExternalForm();
        }
        catch (MalformedURLException e)
        {
            logger.log(Level.WARNING, "", e);
        }
        return null;
    }


    @Override
    public String toString()
    {
        return this.name;
    }
}
