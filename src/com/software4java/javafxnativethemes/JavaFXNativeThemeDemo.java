package com.software4java.javafxnativethemes;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: TB
 * Date: 20.02.13
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
public class JavaFXNativeThemeDemo extends Application
{
    @Override
    public void start(Stage stage) throws Exception
    {
        JavaFXNativeThemeScene javaFXNativeThemeScene = new JavaFXNativeThemeScene();
        stage.setScene(javaFXNativeThemeScene.getScene());

        stage.centerOnScreen();
        stage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
