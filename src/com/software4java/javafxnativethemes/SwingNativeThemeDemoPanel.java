package com.software4java.javafxnativethemes;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: TB
 * Date: 20.02.13
 * Time: 23:17
 * To change this template use File | Settings | File Templates.
 */
public class SwingNativeThemeDemoPanel extends JPanel
{
    private JButton bDefault;

    public SwingNativeThemeDemoPanel()
    {
        JPanel pHeader = new JPanel(new MigLayout("flowy,ins 0", "20:push[]20:push", "[41!]"));
        pHeader.setBackground(new Color(0xCCCCCC));
        JLabel l = new JLabel("Swing");
        JComboBox skinChooser = new JComboBox(new String[] { "Mac OS X", "Windows" });
        l.setFont(l.getFont().deriveFont(24f));

        pHeader.add(l, "center, w pref!");
        pHeader.add(skinChooser, "center, w pref!");

        //AquaButtonBorder

        JPanel pButtons = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lButtons = new JLabel("Buttons");
        JButton bNormal = new JButton("Normal");

        JToggleButton bSelected = new JToggleButton("Selected");
        bSelected.setSelected(true);
        this.bDefault = new JButton("Default");
        pButtons.add(lButtons);
        pButtons.add(bNormal, "cell 0 1");
        pButtons.add(bSelected, "cell 0 1");
        pButtons.add(bDefault, "cell 0 1");

        JPanel pCheckBoxes = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lCheckBoxes = new JLabel("CheckBoxes");
        JCheckBox cbNormal = new JCheckBox("Normal");
        JCheckBox cbSelected = new JCheckBox("Default");
        cbSelected.setSelected(true);
        pCheckBoxes.add(lCheckBoxes);
        pCheckBoxes.add(cbNormal, "cell 0 1");
        pCheckBoxes.add(cbSelected, "cell 0 1");

        JPanel pRadiobutton = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lRadiobutton = new JLabel("RadioButton");
        JRadioButton rbNormal = new JRadioButton("Normal");
        JRadioButton rbSelected = new JRadioButton("Default");
        rbSelected.setSelected(true);
        pRadiobutton.add(lRadiobutton);
        pRadiobutton.add(rbNormal, "cell 0 1");
        pRadiobutton.add(rbSelected, "cell 0 1");

        JPanel pTextfields = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lTextfields = new JLabel("Textfields");
        JTextField tf = new JTextField("Thats a default textfield.");
        pTextfields.add(lTextfields);
        pTextfields.add(tf, "cell 0 1");

        final JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem mi1 = new JMenuItem("Cut");
        JMenuItem mi2 = new JMenuItem("Copy");
        JMenuItem mi3 = new JMenuItem("Paste");
        JMenuItem mi4 = new JMenuItem("Select all");
        popupMenu.add(mi1);
        popupMenu.add(mi2);
        popupMenu.add(mi3);
        popupMenu.add(new JSeparator());
        popupMenu.add(mi4);

        JPanel pContextMenu = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lContextMenu = new JLabel("ContextMenu");
        final JButton bContextMenu = new JButton("Press for Context Menu");
        bContextMenu.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                popupMenu.show(bContextMenu, 0, 0);
            }
        });
        bContextMenu.setComponentPopupMenu(popupMenu);

        pContextMenu.add(lContextMenu);
        pContextMenu.add(bContextMenu, "cell 0 1");

        JPanel pSlider = new JPanel(new MigLayout("", "[]", "[]"));
        JLabel lSlider = new JLabel("Slider");
        JSlider slider = new JSlider();
        pSlider.add(lSlider);
        pSlider.add(slider, "cell 0 1");


        JPanel pContent = new JPanel(new MigLayout("ins 0, flowy", "[]", "[]"));
        pContent.add(pButtons, "");
        pContent.add(pCheckBoxes, "");
        pContent.add(pRadiobutton, "");
        pContent.add(pTextfields, "");
        pContent.add(pContextMenu, "");
        pContent.add(pSlider, "");


        this.setLayout(new MigLayout("flowy, ins 0 10 10 10", "[fill,grow]", "[]"));
        this.add(pHeader, "");
        this.add(pContent, "");

        Font f = UIManager.getFont("Button.font");
        System.out.println("f = " + f);

        this.addAncestorListener(new AncestorListener()
        {
            @Override
            public void ancestorAdded(AncestorEvent event)
            {
                ((JFrame)event.getAncestor()).getRootPane().setDefaultButton(bDefault);
            }

            @Override
            public void ancestorRemoved(AncestorEvent event)
            {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void ancestorMoved(AncestorEvent event)
            {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }
}

