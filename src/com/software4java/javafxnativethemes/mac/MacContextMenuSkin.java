package com.software4java.javafxnativethemes.mac;

import com.sun.javafx.scene.control.skin.ContextMenuSkin;
import javafx.scene.control.ContextMenu;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;

/**
 * User: TB
 * Date: 04.04.12
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
public class MacContextMenuSkin extends ContextMenuSkin
{
    public MacContextMenuSkin(ContextMenu contextMenu)
    {
        super(contextMenu);

        System.out.println("MaccontextMenu");

        DropShadow effect = new DropShadow();

        effect.setOffsetY(10);
        effect.setOffsetX(0);

        effect.setRadius(20);


        effect.setColor(new Color(0, 0, 0, .3));
        getNode().setEffect(effect);
    }
}
